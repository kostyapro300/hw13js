document.addEventListener("DOMContentLoaded", function () {
    const images = document.querySelectorAll(".image-to-show");
    const startButton = document.getElementById("startSlideshow");
    const stopButton = document.getElementById("stopSlideshow");
    let currentIndex = 0;
    let intervalId;
    // делаем так что бы отображалася тока 1 картинка по индексу. (то есть, если индекс = 1, то показываеться первая картинка и т.д)
    function showImage(index) {
        images.forEach((image, i) => {
            if (i === index) {
                image.style.display = "block";
            } else {
                image.style.display = "none";
            }
        });
    }

    function startSlideshow() { 
        showImage(currentIndex); // вызываем прошлую фунцию передавая в нее текущий индекс
        intervalId = setInterval(() => { // делаем интервал, который каждые 3 секунды прибавляет индекс+1 для того что бы картинки менялись
            currentIndex = (currentIndex + 1) % images.length;
            showImage(currentIndex);
        }, 3000);
        startButton.disabled = true;  // делаем кнопку не активной после того как слайд шоу начнеться(если так не сделать, то если нажать на кнопку начать много раз сразу то все сломаеться )
        stopButton.disabled = false;  
    }

    function stopSlideshow() {
        clearInterval(intervalId);
        intervalId = null // приравниваю к null что бы не было удвоение tru false
        startButton.disabled = false; 
        stopButton.disabled = true; 
    }

    startButton.addEventListener("click", startSlideshow);
    stopButton.addEventListener("click", stopSlideshow);
});